// ==UserScript==
// @name       RT ticket Cleaner-upper
// @namespace  http://nickshontz.com
// @version    0.1.54
// @description  script for cleaning up extranious junk in RT
// @match      http://rt.ito.umt.edu/*
// @copyright  2012+, You
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js
// ==/UserScript==
jQuery(document).ready(function($){

	/** focusing on things **/
	$(".messagebox").focus();

	/** Cleaning up Ticket History **/    
	$("#ticket-history .other, #ticket-history .people, #ticket-history .basics").css("display","none");
	var show_all_history_text = "Show all History";
	var hide_all_history_text = "Hide all History";
	$(".history .titlebox-title .right").prepend("<a href='#' class='toggle_all_history closed'>"+show_all_history_text+"</a> --");
	$(".toggle_all_history").live("click",function(){
		if($(this).hasClass('closed'))  {
			$(this).removeClass('closed').addClass('open');
			$(this).html(hide_all_history_text);
		} else {
			$(this).removeClass('open').addClass('closed');
			$(this).html(show_all_history_text);
		}
		$("#ticket-history .other, #ticket-history .people, #ticket-history .basics").toggle();
        return false;
	});

	/** Cleaning up Navigation **/
	var actions = $("#li-page-actions ul").clone();
	actions.prepend("<li><strong><a href='#' class='menu-item'>Ticket Actions:</a></strong></li>");
	$(actions).attr("style","clear:both").addClass($("#page-menu").attr("class"));
	$("li",actions).attr("style","background-color:transparent;");
	$("li a",actions).attr("style","padding-top: 0px; padding-bottom: 6px;");
	$(".sf-menu").css("margin-bottom","0");
	$("#page-navigation").append(actions);

	/** Cleaning up Metadata **/
	var basics = $("<div class='ticket-info-basics'><div class='ticket-info-basics titlebox rolled-up'>"+$(".ticket-info-basics .ticket-info-basics").clone().html()+"</div></div>");
	var custom_fields = $("<div class='ticket-info-cfs'><div class='ticket-info-cfs titlebox'>"+$(".ticket-info-cfs .ticket-info-cfs").clone().html()+"</div></div>");
	$(".ticket-summary td.boxcontainer:first .ticket-info-basics").remove();
	$(".ticket-summary td.boxcontainer:first .ticket-info-cfs").remove();
	$(".ticket-summary td.boxcontainer:first").append(basics);
	$(".ticket-summary td.boxcontainer:last").prepend(custom_fields);
	$(".summary .ticket-info-attachments .widget a").click();
	$(".summary .ticket-info-basics .widget a").click();
	$(".summary .ticket-info-requestor .widget a").click();
	$(".summary .ticket-info-links .widget a").click();
	$(".summary .ticket-info-dates .widget a").click();
	$(".summary .ticket-info-reminders .widget a").click();
});
